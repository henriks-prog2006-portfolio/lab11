# Lab11 - Bearing in Space (Rust)





----------------
### Prelim. info

*Crates used for this program:*

- regex = "1"


# The task: Space Voyager

The task is to develop a program to read standard input,
extract the numerical values, and use them to calculate a number.
> - Write a program to read from the standard input. Each line of input provides a single inscription.
> - Each line consists of a series of alpha-numerical symbols. The first and the last decimal digit represent the left and right parts of the two-digit numerical value in decimal.
> - Some of the digits are actually spelled out with letters: one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".

#### Example Input:
```
two3nine
eightwofour
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
```
#### Output:
```
282
```
### Functions implemented:
The program only has one function to do the heavy lifting and a main to put it together
#### extract_numbers(re: &Regex, input: &str) -> (Option<u32>, Option<u32>)
Extracts the first and last numeric values from a line using the provided regex.
