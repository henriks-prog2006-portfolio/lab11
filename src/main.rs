use regex::Regex;
use std::io::{self, BufRead};

fn main() {
    let regex_pattern = r"[0-9]|zero|one|two|three|four|five|six|seven|eight|nine";
    let re = Regex::new(regex_pattern).unwrap();
    let stdin = io::stdin();
    let reader = stdin.lock();
    let mut sum = 0;

    for line in reader.lines().filter_map(Result::ok) {
        let (first, last) = extract_numbers(&re, &line);
        if let (Some(f), Some(l)) = (first, last) {
            let two_digit_number = f * 10 + l;
            println!("Processed number: {}", two_digit_number);
            sum += two_digit_number;
        }
    }

    let final_value = sum % 360;
    println!("Final Value: {}", final_value);
}

/// Extracts the first and last numeric values from a line using the provided regex.
fn extract_numbers(re: &Regex, input: &str) -> (Option<u32>, Option<u32>) {
    let mut first_number: Option<u32> = None;
    let mut last_number: Option<u32> = None;

    for cap in re.captures_iter(input) {
        let num_str = cap.get(0).unwrap().as_str();
        let num = match num_str {
            "zero" => 0,
            "one" => 1,
            "two" => 2,
            "three" => 3,
            "four" => 4,
            "five" => 5,
            "six" => 6,
            "seven" => 7,
            "eight" => 8,
            "nine" => 9,
            _ => num_str.parse().unwrap(), // Parses single-digit numbers
        };

        if first_number.is_none() {
            first_number = Some(num);
        }
        last_number = Some(num); // Updates with each match found
    }

    (first_number, last_number)
}
